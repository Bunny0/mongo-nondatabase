package net.codejava;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.Scanner;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

public class MongoInsert {

	public static void main(String[] args) throws UnknownHostException {

		Scanner kb = new Scanner(System.in);
		// menu
		System.out.println("********** Data Management for BigC Customer *************");
		System.out.println("MENU\r\n" + "0. Exit\r\n" + "1. Showing All of Databases Name\r\n"
				+ "2. Showing All of Document of the Customer Collection\r\n" + "3. Adding the Person information\r\n"
				+ "4. Editing the Person information\r\n" + "5. Removing the Person information");
		// input
		System.out.print("Input Number : ");
		int number = kb.nextInt();

		while (number != 0) {
			// Creating a Mongo client
			MongoClient mongo = new MongoClient("localhost", 27017);

			DB db = mongo.getDB("BigC");
			DBCollection table = db.getCollection("customers");
			// Show All of Databases Name
			List<String> dbs = mongo.getDatabaseNames();
			// set name in collection
			Set<String> tables = db.getCollectionNames();
			// input name in collection
			BasicDBObject document = new BasicDBObject();
			// edit name or age in collection
			BasicDBObject query = new BasicDBObject();
			// remove
			BasicDBObject searchQuery = new BasicDBObject();
			switch (number) {
			// case01
			case 1:
				System.out.println("********** Show All of Databases Name *************");
				for (String namelist : dbs) {
					System.out.println(namelist);
				}
				break;
			// case02
			case 2:
				System.out.println("********* Showing All of Document of the Customer Collection ********");
				for (String coll : tables) {
					System.out.println(coll);
				}
				break;
			// case03
			case 3:
				System.out.println("************ Adding the Person information ************************");
				// input name
				System.out.print("Name :");
				String name = kb.next();
				// input age
				System.out.print("Age :");
				int age = kb.nextInt();
				document.put("name", name);
				document.put("Age", age);
				table.insert(document);
				System.out.println("Success");
				break;
			// case04
			case 4:
				System.out.println("************ Editing the Person information *************************");
				// input name0
				System.out.print("Name :");
				String name0 = kb.next();
				query.put("name", name0);
				// input age0
				System.out.print("Age :");
				int age0 = kb.nextInt();
				BasicDBObject newDocument = new BasicDBObject();
				newDocument.put("age", age0);
				BasicDBObject updateObj = new BasicDBObject();
				updateObj.put("$set", newDocument);
				table.update(query, updateObj);
				break;
			// case 5
			case 5:
				System.out.println("************ Removing the Person information *************************");
				// input name1
				System.out.print("Name :");
				String name1 = kb.next();
				searchQuery.put("name", name1);
				table.remove(searchQuery);
				break;

			}

			System.out.print("Input Number : ");
			number = kb.nextInt();
		}
		System.out.println("********** Exit *************\r\n" + "Thank you for Data Managing\r\n" + "Goodbye");

	}

}
